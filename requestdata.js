let { getJsDateFromExcel } = require('excel-date-to-js');
let Coordinates = require('coordinate-parser');
let moment = require('moment');

module.exports = (column, cont) => {
    // console.log(!!(column[4] && column[5]));
    let position = {};
    position['latitude'] = 0;
    position['longitude'] = 0;
  if(!!(column[4] && column[5])) {
    try {
        position = new Coordinates(`${column[4]} ${column[5]}`);
    } catch (error) {
     
    }
  }
 
  let dateInspectionFormated;
  try {
    dateInspectionFormated = moment(getJsDateFromExcel(column[7])).add(1, 'day').format("DD/MM/YYYY");
  } catch (error) {
    throw error;
  } 
  
  return {
    "name" : `PONTE_${column[2]}_${column[0]}_${dateInspectionFormated}`,
    "inspectorId" : `${dateInspectionFormated}`,
    "form" : {
        "asset" : {
            "Tipo de ativo" : "Ponte",
            "Km do ativo" : column[0] || '',
            "Par de estação" : column[3] || '',
            "Supervisão": column[2] || '',
            "Nome do ativo": `PONTE_${column[2]}_${column[0]}_${dateInspectionFormated}`,
            "Localização" : {
                "type" : "GPS",
                "latitude" : position.latitude || '' ,
                "longitude" : position.longitude || '',
                "accuracy" : "Entrada manual"
            },
            "Altitude (m)" : column[6] || '',
            "TB" : column[9] || '',
            "Número de vãos" : column[11] || '',
            "Contratrilho" : column[12] || '',
            "Contratrilho interno (m)" : column[13] || '',
            "Contratrilho externo (m)" : column[14] || '',
            "Fita de travamento" : column[15] || '',
            "Juntas E01" : column[16] || '',
            "Juntas de ligação sobre o ativo" : '',
            "Juntas E02" : column[18] || '',
            "Estrado" : column[19] || '',
            "Dormente padrão" : column[21] || '',
            "Dormentes sobre a OAE" : column[22] || '',
            "Dormentes Inservíveis" : column[23] || '',
            "Tabuleiro" : column[24] || '',
            "Passagem inferior" : column[25] || '',
            "Criticidade do trecho" : column[26] || '',
            "Viga contínua" : column[27] || '',
            "Passarela" : column[30] || '',
            "Idade" : column[28] || '',
            "Estado" : "ATIVO",
            "Área urbana" : column[29] || '',
            "Carga contaminante" : column[31] || '',
            "Maior vão (m)" : column[37] || '',
            "Espelho a espelho (cm)" : column[38] || '',
            "Tipo de estrutura" : '',
            "NA (m)" : column[40] || '',
            "Geometria da via" : column[43] || '',
            "Esconcidade" : column[44] || '',
            "Par 1" : column[45] || '',
            "Altura do espelho 1 (m)" : column[46] || '',
            "Largura do apoio 1 (m)" : column[47] || '',
            "Altura do encontro 1 (m)" : column[48] || '',
            "Apoio 1 (m)" : column[49] || '',
            "Apoio ao topo do trilho 1 (m)" : column[50] || '',
            "Par 2" : column[51] || '',
            "Altura do espelho 2 (m)" : column[52] || '',
            "Largura do apoio 2 (m)" : column[53] || '',
            "Altura do encontro 2 (m)" : column[54] || '',
            "Apoio 2 (m)" : column[55] || '',
            "Apoio ao topo do trilho 2 (m)" : column[56] || '',
            "Detector de descarrilhamento" : '',
            "APP" : '',
            "APA" : ''
        },
        "inspection" : {
            "Data de inspeção" : dateInspectionFormated
        }
    },
  }

}
