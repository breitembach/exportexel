var fs = require('fs');

// if (process.argv.length <= 2) {
//   console.log("Usage: " + __filename + " files/");
//   process.exit(-1);
// }

// var path = process.argv[2];

module.exports = function readFiles(dirname) {
  return new Promise(function (resolve, reject) {
    fs.readdir(dirname, function(err, file) {
      if(err) reject(err);
      let filesPath = [];
      for (let i = 0; i < file.length; i++) {
        filesPath.push(dirname + file[i]);
      }
      resolve(filesPath);
    });
  });
}
