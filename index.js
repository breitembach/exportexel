let readXlsxFile = require('read-excel-file/node');
let parseDataSend = require('./requestdata');
let  = require('./requestdata');
let readFiles = require('./readfiles');
let axios = require('axios');
let ProgressBar = require('ascii-progress');

let { getJsDateFromExcel } = require('excel-date-to-js');
let Coordinates = require('coordinate-parser');
let moment = require('moment');

let sendDataToServer = require("./sendData");


const baseURL = "URLHERE";

async function dataExportExel () {
    try {
      
    
    let position = {};
    let filesPath = await readFiles('files/');
    let dataIsEmpty = true;
    let alturaDoEspelhoIsEmpty = true;
    let rowTH;
    let promises = [];
    let d =[];
    for (const key in filesPath) {
      if (filesPath.hasOwnProperty(key)) {
        console.log( "Lendo- "+ filesPath[key]);
        let rows = await readXlsxFile(filesPath[key], {sheet: 2});
        // console.log(rows.length)
        // console.log("Leitura TH",rows[0][0]);
        // console.log("Leitura de CADASTRO Trash",rows[0][0].toUpperCase() == 'CADASTRO');
        let index;
        if(rows[0][0].toUpperCase() == 'KM') {
          index = 1;
          rowTH = 0;
        } else if(rows[0][0].toUpperCase() == 'CADASTRO') {
          rowTH = 1;
          index = 2;

        }else {
          index = 0;
        }
        for (index; index < rows.length; index++) {
          
          // bar.tick(index);
          let dateInspectionFormated;
          // console.log(!rows[index] == null)
          if(!rows[index]) {
            continue;
          }
          let createObj = {
            "planilha": true,
            "name": '',
            "inspectorId": '',
            'form': {
              'asset': {
                "Tipo de ativo" : "Ponte",
                "Km do ativo" : '',
                "Par de estação" : '',
                "Supervisão": '',
                "Nome do ativo": '',
                "Localização" : {
                    "type" : "GPS",
                    "Latitude" : '' ,
                    "Longitude" : '',
                    "Altitude": '',
                    "Precisão" : "Entrada manual"
                },
                "TB" : '',
                "Número de vãos" :  '',
                "Contratrilho" :  '',
                "Contratrilho interno (m)" :  '',
                "Contratrilho externo (m)" :  '',
                "Fita de travamento" :  '',
                "Juntas E01" :  '',
                "Juntas de ligação sobre o ativo" : '',
                "Juntas E02" :  '',
                "Estrado" :  '',
                "Dormente padrão" :  '',
                "Dormentes sobre a OAE" :  '',
                "Dormentes Inservíveis" :  '',
                "Tabuleiro" :  '',
                "Passagem inferior" :  '',
                "Criticidade do trecho" :  '',
                "Viga contínua" :  '',
                "Passarela" :  '',
                "Idade" :  '',
                "Estado" : "ATIVO",
                "Área urbana" :  '',
                "Carga contaminante" :  '',
                "Maior vão (m)" :  '',
                "Espelho a espelho (cm)" :  '',
                "Tipo de estrutura" : '',
                "NA (m)" :  '',
                "Geometria da via" :  '',
                "Esconcidade" :  '',
                "Par 1" :  '',
                "Altura do espelho 1 (m)" :  '',
                "Largura do apoio 1 (m)" :  '',
                "Altura do encontro 1 (m)" :  '',
                "Apoio 1 (m)" :  '',
                "Apoio ao topo do trilho 1 (m)" :  '',
                "Par 2" :  '',
                "Altura do espelho 2 (m)" :  '',
                "Largura do apoio 2 (m)" :  '',
                "Altura do encontro 2 (m)" :  '',
                "Apoio 2 (m)" :  '',
                "Apoio ao topo do trilho 2 (m)" :  '',
                "Detector de descarrilhamento" : '',
                "APP" : '',
                "APA" : ''
              },
              'inspection': {
                'Data de inspeção':''
              }
            }
          };
          for (let [thPos, th] of rows[rowTH].entries()) {
            
            // let tmp = {};
            position = {};
            // console.log(th)
            // console.log("DATA: ", thPos)
            // console.log(rows[index][thPos] == null);
            
            // if(rows[index][thPos] == null) {
            //   continue;
            // }
            if((th != null || th != ''))
            switch (th) {
              case "KM":
                  createObj['name'] = `${rows[index][thPos]}`
                  createObj['form']['asset']['Km do ativo'] = rows[index][thPos] || '';
                  
                  break;
                case "DATA": 
                  try {

                      if(rows[index][thPos]){
                        
                        if(dataIsEmpty) {
                          dateInspectionFormated = moment(rows[index][thPos]).format("DD/MM/YYYY") //resolved bug day
                          createObj['name'] = `${createObj['name']}_${dateInspectionFormated}`;
                          createObj['inspectorId'] = `${dateInspectionFormated}`;
                          createObj['form']['inspection']['Data de inspeção'] = dateInspectionFormated;
                          createObj['form']['asset']['Nome do ativo'] = createObj['name'];
                          dataIsEmpty = false;
                        }
                      }
                    } catch (error) {
                      throw error;
                    } 
                  break;
                case "SB":
                  createObj['form']['asset']['Par de estação'] = rows[index][thPos];
                  break;
                case "SUPERVISÃO":
                  createObj['name'] = `PONTE_${rows[index][thPos]}_${createObj['name']}`
                  createObj['form']['asset']['Supervisão'] = rows[index][thPos];
                  break;
                case "ALTITUDE":
                  createObj['form']['asset']['Localização']['Altitude'] = rows[index][thPos] || '';
                  break;
                
                case "LATITUDE":
               
                  createObj['form']['asset']['Localização']['Latitude'] = rows[index][thPos] || '';
                    break;
                case "LONGITUDE":
                createObj['form']['asset']['Localização']['Longitude'] = rows[index][thPos] || '';
               
                break;

                case "TB":
                  createObj['form']['asset']['TB'] = rows[index][thPos] || '';
                break;
                case "VÃOS":
                createObj['form']['asset']['Número de vãos'] = rows[index][thPos] || '';
                break;
                case "CONTRATRILHO":
                createObj['form']['asset']['Contratrilho'] = rows[index][thPos] || '';

                break;
                case "CONTRATRILHO INTERNO (m)":
                case "CONTRATRILHO INTERNO (m) APLICAR":
                createObj['form']['asset']['Contratrilho interno (m)'] = rows[index][thPos] || '';

                break;
                case "CONTRATRILHO EXTERNO (m)":
                case "CONTRATRILHO EXTERNO (m) APLICAR":
                createObj['form']['asset']['Contratrilho externo (m)'] = rows[index][thPos] || '';

                break;
                case "FITA DE TRAVAMENTO":
                createObj['form']['asset']['Fita de travamento'] = rows[index][thPos] || '';

                break;
                case "JUNTAS E01":
                createObj['form']['asset']['Juntas E01'] = rows[index][thPos] || '';

                break;
                case "JUNTAS OAE":
                createObj['form']['asset']['Juntas de ligação sobre o ativo'] = rows[index][thPos] || '';

                break;
                case "ÁREA URBANIZADA":
                createObj['form']['asset']['Área urbana'] = rows[index][thPos] || '';
                break;
                
                case "JUNTAS E02":
                createObj['form']['asset']['Juntas E02'] = rows[index][thPos] || '';

                break;
                case "ESTRADO":
                createObj['form']['asset']["Estrado"] = rows[index][thPos] || '' || '';

                break;
                case "DORMENTE PADRÃO":
                createObj['form']['asset']['Dormente padrão'] = rows[index][thPos] || '';

                break;
                case "DORMENTES SOBRE A OAE":
                createObj['form']['asset']['Dormentes sobre a OAE'] = rows[index][thPos] || '';

                break;
                case "DORMENTES INSERVÍVEIS":
                createObj['form']['asset']['Dormentes Inservíveis'] = rows[index][thPos] || '';

                break;
                case "TABULEIRO":
                createObj['form']['asset']['Tabuleiro'] = rows[index][thPos] || '';

                break;
                case "PASSAGEM INFERIOR":
                createObj['form']['asset']['Passagem inferior'] = rows[index][thPos] || '';

                break;
                case "CRITICIDADE TRECHO":
                createObj['form']['asset']['Criticidade do trecho'] = rows[index][thPos] || '';

                break;
                case "VIGA CONT,":
                createObj['form']['asset']['Viga contínua'] = rows[index][thPos] || '';

                break;
                case "PASSARELA":
                createObj['form']['asset']['Passarela'] = rows[index][thPos] || '';

                break;
                case "IDADE (Maior que 50 anos?)":
                createObj['form']['asset']['Idade'] = rows[index][thPos] || '';

                break;
                case "IDADE (Maior que 50 anos?)":
                createObj['form']['asset']['Área urbana'] = rows[index][thPos] || '';

                break;
                case "CARGA CONT.":
                createObj['form']['asset']['Carga contaminante'] = rows[index][thPos] || '';

                break;
                case "MAIOR VÃO (m)":
                createObj['form']['asset']['Maior vão (m)'] = rows[index][thPos] || '';

                break;
                case "ESPELHO A ESPELHO (cm)":
                createObj['form']['asset']['Espelho a espelho (cm)'] = rows[index][thPos] || '';

                break;
                case "N.A. (m)":
                createObj['form']['asset']['NA (m)'] = rows[index][thPos] || '';

                break;
                case "GEOMETRIA":
                createObj['form']['asset']['Geometria da via'] = rows[index][thPos] || '';

                break;
                case "ESCONSIDADE":
                createObj['form']['asset']['Esconcidade'] = rows[index][thPos] || '';

                break;
                case "PAR 1":
                createObj['form']['asset']['Par 1'] = rows[index][thPos] || '';

                break;
                case "ALTURA DO ESPELHO (cm)":
                  if(alturaDoEspelhoIsEmpty){
                    createObj['form']['asset']['Altura do espelho 1 (m)'] = rows[index][thPos] || '';
                    alturaDoEspelhoIsEmpty = false;
                  } else{ 
                    createObj['form']['asset']['Altura do espelho 2 (m)'] = rows[index][thPos] || '';
                  }
                break;
                case "LARGURA APOIO I (cm)":
                createObj['form']['asset']['Largura do apoio 1 (m)'] = rows[index][thPos] || '';

                break;
                case "ALTURA DO ENCONTRO I (cm)":
                createObj['form']['asset']['Altura do encontro 1 (m)'] = rows[index][thPos] || '';

                break;
                case "APOIO I (cm)":
                createObj['form']['asset']['Apoio 1 (m)'] = rows[index][thPos] || '';

                break;
                
                case "APOIO AO TOPO DO TRILHO I (cm)":
                createObj['form']['asset']['Apoio ao topo do trilho 1 (m)'] = rows[index][thPos] || '';

                break;
                case "PAR 2":
                createObj['form']['asset']['Par 2'] = rows[index][thPos] || '';

                break;
                case "LARGURA APOIO II (cm)": 
                createObj['form']['asset']['Largura do apoio 2 (m)'] = rows[index][thPos] || '';

                break;
                case "ALTURA DO ENCONTRO II (cm)": 
                createObj['form']['asset']['Altura do encontro 2 (m)'] = rows[index][thPos] || '';

                break;
                case "APOIO II (cm)": 
                createObj['form']['asset']['Apoio 2 (m)'] = rows[index][thPos] || '';

                break;
                case "APOIO AO TOPO DO TRILHO II (cm)": 
                createObj['form']['asset']['Apoio ao topo do trilho 2 (m)'] = rows[index][thPos] || '';

                break;
                
                default:
                  break;
              }
             
            }// foreach th
            dataIsEmpty = true;
            alturaDoEspelhoIsEmpty = true;
          if(!(createObj['form']['asset']['Km do ativo'] == '' || createObj['form']['asset']['Km do ativo'] == null)){
            // console.log(createObj['form']['asset']['Supervisão'])
            // if(createObj['form']['asset']['Supervisão'] == 'Alagoinhas') {
              await axios.post(baseURL, createObj);
              // console.log(createObj['form']['asset']['Km do ativo']);
            // }

          }
         
          
          // console.log('====================================');
          // console.log(createObj);
          // console.log('====================================');
          
          // d.push(parseDataSend(rows[index], index))
           
          // promises.push(await sendDataToServer(rows[index], index)); // not working

        }
      }
    } // end Files

    // await Promise.all(promises);
  } catch (error) {
      console.log(error)
  }

}

dataExportExel();