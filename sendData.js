
let parseDataSend = require('./requestdata');

const baseURL = "URLHERE"
let axios = require('axios');


module.exports = (rows, index) => {
  return new Promise( (resolve, reject) => {
    axios.post(baseURL, parseDataSend(rows[index], index))
      .then(res => { resolve() })
      .catch(err => { reject(err) });
  });
}